# GL C BaseCamp Task1

1st task of GlobalLogic C/C++ BaseCamp

Client/Server application to close processes on remote HOST.
- client gets processes list and sends them by server request.
- server will request to close one of the processes on client's host.
- client has to reply with success or failure.


Contents of repository:

/TCP_Server - project of TCP Server. Compiles both on Windows (Visual Studio) and Ubuntu (with gcc makefile).
Files:
- linux_socket.h and linux_socket.c: describe user functions to work with sockets on linux;
- windows_socket.h and windows_socket.c: describe user functions to work with sockets on windows (Winsock);
- main.h: determines OS with #ifdef and includes libraries to work either with linux sockets or winsock;
- main_server.c: main() function;
- makefile: to compile project on linux.  

/TCP_Client - project of TCP Client. Compiles both on Windows (Visual Studio) and Ubuntu (with gcc makefile).
Files:
- linux_socket.h and linux_socket.c: describe user functions to work with sockets on linux;
- windows_socket.h and windows_socket.c: describe user functions to work with sockets on windows (Winsock);
- linux_process.h and linux_process.c: describe functions GetProcessList() and CloseProcess() for linux; 
- windows_process.h and windows_process.c: describe functions GetProcessList() and CloseProcess() for windows; 
- main.h: determines OS with #ifdef and includes libraries to work either with linux sockets or winsock;
- main_client.c: main() function;
- makefile: to compile project on linux.

WBS.txt - description of initial code structure for both Client & Server API; breakdown of tasks into minor subtasks.


Communication workflow:
1. TCP_Server launch
2. TCP_Client launch
3. Server & Client connect with each other, Server waits for user entry
4. User enters 1 of 2 commands using following format:
- get process list 
- close process <PID>
5. Server sends this command to client
6. Client processes incoming data, check whether it understands command
6.1. If Client processed it well, it executes corresponding function & transfer back results to Server
6.2. If Client didn't understand command (because of user mistake, for instance), it sends back an error message
7. Server prints received data from Client & asks user for another entry -> loop from p.4
