#include "linux_process.h"

#ifdef __linux__

int filter(const struct dirent *dir)
{
     return !fnmatch("[1-9]*", dir->d_name, 0);
}

unsigned int GetProcessList(char* buffer, unsigned int buf_size)
{
    char* buf_ptr = buffer;
    int size_left = buf_size;
    unsigned int current_buf_size = 0;

    struct dirent **namelist;
    int n;

    n = scandir("/proc", &namelist, filter, 0);
    if (n < 0)
    {
        puts("Not enough memory.");
    }
    else
    {
        //while(n--)
        for (unsigned int i = 0; i < n; i++)
        {
	       //processdir(namelist[n]);
	    struct dirent *dir = namelist[i];
	    int name_len, pid_len;

	    char path_to_file[32];
            strcpy(path_to_file, "/proc/");
            strcat(path_to_file, dir->d_name);
            strcat(path_to_file, "/comm");

            FILE *fp;
            char process_name[PROCESS_NAME_SIZE];
            fp = fopen(path_to_file, "r");
            fgets(process_name, 64, fp);

            //printf("%7s %s\n",dir->d_name, process_name);
            snprintf(buf_ptr, size_left, "%7s ", dir->d_name);
            pid_len = 8;
            current_buf_size += pid_len;
            buf_ptr = buf_ptr + pid_len;
            size_left = size_left - pid_len;

            snprintf(buf_ptr, size_left, "%s\n", process_name);
            name_len = strlen(process_name);
            current_buf_size += (name_len);
            buf_ptr = buf_ptr + (name_len);
            size_left = size_left - (name_len);

            fclose(fp);
	    free(namelist[i]);
        }
	free(namelist);
    }
    return current_buf_size;
}
unsigned int CloseProcess(char* buffer, unsigned int process_id)
{
    //system("kill <process_id> &> output.txt");
    char command[128];
    snprintf(command, 128, "%s %d %s\n", "kill ", process_id, "2>&1 | tee output.txt");
    system(command);

    FILE * ofp;
    char out_file[] = "output.txt";

    ofp = fopen(out_file, "r+");

    if (ofp == NULL)
    {
        fprintf(stderr, "Can't open output file %s!\n", out_file);
        exit(1);
    }

    fgets(buffer, 128, ofp);
    int str_len = strlen(buffer);
    if (str_len == 0)
    {
        snprintf(buffer, 128, "%s %d %s\n", "SUCCESS: process", process_id, "killed successfully!");
        str_len = strlen(buffer);
    }

    fclose(ofp);

    //printf("\n%d\n", str_len);

    return str_len;
}

#endif
