#ifndef WINDOWS_PROCESS_H
#define WINDOWS_PROCESS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32

#define PROCESS_NAME_SIZE  128

//#define UNICODE
#include <Windows.h>
#include <psapi.h>
#include <tlhelp32.h>

unsigned int GetProcessList(char*, unsigned int);
unsigned int CloseProcess(char*, unsigned int);

#endif

#endif
