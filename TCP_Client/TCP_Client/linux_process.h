#ifndef LINUX_PROCESS_H
#define LINUX_PROCESS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef __linux__

#define PROCESS_NAME_SIZE  128

#include <dirent.h>
#include <fnmatch.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

unsigned int GetProcessList(char*, unsigned int);
unsigned int CloseProcess(char*, unsigned int);

#endif

#endif
