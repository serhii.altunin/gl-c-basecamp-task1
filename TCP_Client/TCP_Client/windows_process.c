#include "windows_process.h"

#ifdef _WIN32

unsigned int GetProcessList(char* buffer, unsigned int buf_size)
{
    unsigned int current_buf_size = 0;
    //wprintf(L"Start:\n");
    HANDLE hndl = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS | TH32CS_SNAPMODULE, 0);
    if (hndl)
    {
        PROCESSENTRY32  process = { sizeof(PROCESSENTRY32) };
        Process32First(hndl, &process);
        char* buf_ptr = buffer;
        int size_left = buf_size;

        do
        {
            char pid_char[6];
            char process_name[PROCESS_NAME_SIZE];
            int name_len, pid_len;

            sprintf_s(pid_char, 6, "%d", process.th32ProcessID);
            wcstombs_s(&name_len, process_name, sizeof(process_name), process.szExeFile, sizeof(process.szExeFile));

            snprintf(buf_ptr, size_left, "%7u ", process.th32ProcessID);
            //printf("%s", buf_ptr);

            //current_buf_size += (sizeof(process.th32ProcessID));
            //pid_len = strlen(pid_char) + 1;
            pid_len = 8;
            current_buf_size += pid_len;
            buf_ptr = buf_ptr + pid_len;
            size_left = size_left - pid_len;

            //printf("%d\n", size_left);

            snprintf(buf_ptr, size_left, "%s\n", process_name);
            //printf("%s\n", buf_ptr);

            current_buf_size += (name_len);
            buf_ptr = buf_ptr + (name_len);
            size_left = size_left - (name_len);

            //printf("%d\n", size_left);
            //wprintf(L"%8u, %s\n", process.th32ProcessID, process.szExeFile);
        } while (Process32Next(hndl, &process));
        //*buf_ptr = '\0';

        CloseHandle(hndl);
    }
    return current_buf_size;
}

unsigned int CloseProcess(char* buffer, unsigned int process_id)
{
    //system("taskkill /PID <process_id> /F > output.txt 2>&1");
    char command[128];
    snprintf(command, 128, "%s %d %s\n", "taskkill /PID", process_id, "/F > output.txt 2>&1");
    system(command);

    FILE * ofp;
    char out_file[] = "output.txt";

    fopen_s(&ofp, out_file, "r+");

    if (ofp == NULL)
    {
        fprintf(stderr, "Can't open output file %s!\n", out_file);
        exit(1);
    }

    fgets(buffer, 128, ofp);
    int str_len = strlen(buffer);

    fclose(ofp);

    //printf("\n%d\n", str_len);

    return str_len;
}

#endif
