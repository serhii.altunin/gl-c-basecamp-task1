#include "main.h"

int is_number(const char*);

int main()
{
	char ipAddress[16] = "127.0.0.1";			// IP Address of the server
	int port = 54000;					// Listening port # on the server

	// Initialize & Create socket
	socket_type sock;
	CreateSocket(&sock); 	//???
	if (sock == -1)
	{
		return -1;
	}

	// Fill in a hint structure
	struct sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(port);
	CreateHint(ipAddress, &hint);

	// Connect to server
	int connectRes = ConnectToServer(&sock, &hint, sizeof(hint));
	if (connectRes == -1)
	{
		CloseSocket(&sock, 1);
		return -3;
	}

	// While loop: accept and echo message back to client
	char buf[BUF_SIZE];
	char get_process_list_cmd[] = "get process list\n";
	char close_process_cmd[] = "close process ";
	const int cmd_len = strlen(close_process_cmd);
	char good_response[] = "Command understood.\n";
	char bad_response[] = "Error! Command not understood. Try again.\n";
	char invalid_arg[] = "Invalid  argument!\n";

	while (1)
	{
		memset(buf, 0, BUF_SIZE);

		int bytesReceived = ReceiveData(&sock, buf);
		if (bytesReceived == -1)
		{
			printf("Error in recv(). Quitting\n");
			break;
		}

		if (bytesReceived == 0)
		{
			printf("Client disconnected \n");
			break;
		}

		//PrintMessage(buf, bytesReceived);
		if (strcmp(buf, get_process_list_cmd) == 0)
		{
			// execute "get process list"
			memset(buf, 0, BUF_SIZE);
			unsigned int buf_sz = GetProcessList(buf, BUF_SIZE);
			//PrintMessage(buf, buf_sz);
			//printf("\n%d\n", buf_sz);

			const int string_len = strlen(good_response);
			
			SendData(&sock, buf, buf_sz + 1);
		}
		else if (strncmp(buf, close_process_cmd, cmd_len) == 0)
		{
			const int string_len = strlen(good_response);
			char* cmd_arg = buf + cmd_len;
			if (is_number(cmd_arg))
			{
				// execute "close process <int>"
				unsigned int pid;
				#ifdef _WIN32
					sscanf_s(cmd_arg, "%d", &pid);
				#else
					sscanf(cmd_arg, "%d", &pid);
				#endif
				memset(buf, 0, BUF_SIZE);
				unsigned int buf_sz = CloseProcess(buf, pid);
				//printf("%d\n", pid);
				
				//PrintMessage(buf, bytesReceived);
				SendData(&sock, buf, buf_sz + 1);
			}
			else
			{
				const int string_len = strlen(invalid_arg);
				PrintMessage(invalid_arg, string_len);
				SendData(&sock, invalid_arg, string_len + 1);
			}
		}
		else
		{
			PrintMessage(bad_response, strlen(bad_response));
			SendData(&sock, bad_response, strlen(bad_response) + 1);
		}

		//SendData(&sock, buf, bytesReceived + 1);
	}

	// Close socket
	CloseSocket(&sock, 1);

	return 0;
}

int is_number(const char* str) 
{
	const int string_len = strlen(str);
	for (int i = 0; i < string_len; ++i) {
		if (!isdigit(str[i]) && i != (string_len - 1))
			return 0;
	}
	return 1;
}
