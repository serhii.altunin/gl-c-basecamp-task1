#ifndef MAIN_H
#define MAIN_H

#include <ctype.h>

#define BUF_SIZE  65535

#ifdef _WIN32
#include "windows_socket.h"
#include "windows_process.h"
#elif __linux__
#include "linux_socket.h"
#include "linux_process.h"
#endif


#endif
