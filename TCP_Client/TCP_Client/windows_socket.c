#include "windows_socket.h"
#include "main.h"

#ifdef _WIN32

int CreateSocket(socket_type* socket_ptr)
{
	// Initialze winsock
	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2);

	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0)
	{
		printf("Can't Initialize winsock! Quitting\n");
		return -1;
	}

	// Create a socket
	*socket_ptr = socket(AF_INET, SOCK_STREAM, 0);
	if (*socket_ptr == INVALID_SOCKET)
	{
		printf("Can't create a socket! Quitting\n");
		return -1;
	}
	return *socket_ptr;
}

void CreateHint(char* ipAddr, struct sockaddr_in* hint_struct_ptr)
{
	inet_pton(AF_INET, ipAddr, &hint_struct_ptr->sin_addr);
}

void BindSocket(socket_type* socket_ptr, struct sockaddr* sock_addr, size_t sz)
{
	bind(*socket_ptr, sock_addr, sz);
}

void SocketListening(socket_type* socket_ptr)
{
	listen(*socket_ptr, SOMAXCONN);
}

void WaitForConnection(socket_type* client_sock_ptr, socket_type* sock_ptr, struct sockaddr_in* client_addr, int* size_addr, char* hst, char* srvc)
{
	*client_sock_ptr = accept(*sock_ptr, (struct sockaddr*)client_addr, size_addr);
	if (getnameinfo((struct sockaddr*)client_addr, sizeof(struct sockaddr_in), hst, NI_MAXHOST, srvc, NI_MAXSERV, 0) == 0)
	{
		printf("%s connected on port %s \n", hst, srvc);
	}
	else
	{
		inet_ntop(AF_INET, &client_addr->sin_addr, hst, NI_MAXHOST);
		printf("%s connected on port %d \n", hst, ntohs(client_addr->sin_port));
	}
}

void CloseSocket(socket_type* socket_ptr, int clean)
{
	closesocket(*socket_ptr);
	if (clean)
	{
		WSACleanup();
	}
}

int ReceiveData(socket_type* socket_ptr, char* buffer)
{
	return recv(*socket_ptr, buffer, BUF_SIZE, 0);
}

void PrintMessage(char* buffer, int bytesRec)
{
	printf("%s", buffer);
}

int SendData(socket_type* socket_ptr, char* buffer, int bytesRec)
{
	return send(*socket_ptr, buffer, bytesRec + 1, 0);
}

int ConnectToServer(socket_type* socket_ptr, struct sockaddr_in* hint_struct_addr, int sz)
{
	return connect(*socket_ptr, (struct sockaddr*)hint_struct_addr, sz);
}

void EnterText(char* user_in)
{
	fgets(user_in, 4096, stdin);
}

#endif
