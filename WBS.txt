Code Structure for Client & Server API:

1. Client
	1.1. Creation & configuration of socket
		1.1.1. Initialize socket
		1.1.2. Create socket
		1.1.3. Bind socket to IP address & port
	1.2. Connect to server, wait for accept
	1.3. Communication loop
		1.3.1. Wait for message (command)
		1.3.2. Execute command locally
		1.3.3. Send results to server (or error message in case of failure)
	1.4. Close & shut down socket
		
2. Server
	2.1. Creation of listening socket
		2.1.1. Initialize socket
		2.1.2. Create socket
		2.1.3. Bind socket to IP address & port
		2.1.4. Configure socket for listening
	2.2. Wait for connection, accept incoming request, close listening socket
	2.3. Communication loop
		2.3.1. Send request to get and transfer processes list
		2.3.2. Wait for response 
		2.3.3. Pick 1 process from the list (or ask user to pick, wait for entry)
		2.3.4. Send request to client to close chosen process (by name or PID)
		2.3.5. Wait for response 
	2.4. Close & shut down socket
	
Tasks Structure:

1. Create TCP client & server API and establish connection between them - 3 days (completed)
	1.1. For Windows OS - 1 day (completed)
		1.1.1. Write code for server
		1.1.2. Write code for client
		1.1.3. Test the connection
	1.2. For Ubuntu - 1 day
		1.2.1. Write code for server
		1.2.2. Write code for client
		1.2.3. Test the connection
	1.3. "Merge" the code, so that it works on both systems - 1 day (completed)
2. Write a program to get a processes list - 1 day
	2.1. For Windows OS
	2.2. For Ubuntu
	2.3. Merge the code
3. Write a program to kill certain process (by name or PID) - 1 day
	3.1. For Windows OS
	3.2. For Ubuntu
	3.3. Merge the code
4. Write a code for client/server communication loop - 1 day
	4.1. Establish a format of "request" from server
	4.2. Write a code for client to process a "request"
	4.3. Write a code for client to send the results of execution
		4.3.1. Processes tree
		4.3.2. Message of successful execution or error message
5. Combine everything together & test the code - 1 day
