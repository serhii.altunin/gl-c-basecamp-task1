#ifndef WINDOWS_SOCKET_H
#define WINDOWS_SOCKET_H

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32

#include <WS2tcpip.h>

#pragma comment (lib, "ws2_32.lib")

typedef SOCKET socket_type;

int CreateSocket(socket_type*);
void CreateHint(char*, struct sockaddr_in*);
void BindSocket(socket_type*, struct sockaddr*, size_t);
void SocketListening(socket_type*);
void WaitForConnection(socket_type*, socket_type*, struct sockaddr_in*, int*, char*, char*);
void CloseSocket(socket_type*, int);
int ReceiveData(socket_type*, char*);
void PrintMessage(char*, int);
int SendData(socket_type*, char*, int);

int ConnectToServer(socket_type*, struct sockaddr_in*, int);
void EnterText(char*);

#endif

#endif
