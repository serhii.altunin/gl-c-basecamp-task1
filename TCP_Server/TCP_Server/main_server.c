#include "main.h"

int main()
{
	// Initialze & create winsock
	socket_type listening;
	CreateSocket(&listening);
	if (listening == -1)
	{
		return -1;
	}

	// Bind the ip address and port to a socket
	char ipAddress[16] = "0.0.0.0";
	int port = 54000;

	// Fill in hint structure
	struct sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(port);
	CreateHint(ipAddress, &hint);

	//Bind socket to IP address & port
	BindSocket(&listening, (struct sockaddr*)&hint, sizeof(hint));

	// Cofigure socket  for listening
	SocketListening(&listening);

	// Wait for a connection
	struct sockaddr_in client;
	int clientSize = sizeof(client);
	socket_type clientSocket;
	char host[NI_MAXHOST];		// Client's remote name
	char service[NI_MAXSERV];	// Service (i.e. port) the client is connect on
	memset(host, 0, NI_MAXHOST);
	memset(service, 0, NI_MAXSERV);
	WaitForConnection(&clientSocket, &listening, &client, &clientSize, host, service);

	// Close listening socket
	CloseSocket(&listening, 0);

	// Do-while loop to send and receive data
	char buf[BUF_SIZE];
	char userInput[4096];
	int bytes_read;

	char get_process_list_cmd[] = "get process list\n";
	char close_process_cmd[] = "close process ";
	const int cmd_len = strlen(close_process_cmd);
	char good_response[] = "Command understood.\n";
	char bad_response[] = "Error! Command not understood. Try again.\n";
	char invalid_arg[] = "Invalid  argument!\n";

	do
	{
		printf(">Enter one of following commands:\n");
		printf("get process list\n");
		printf("close process <PID>\n");
		EnterText(userInput);
		bytes_read = strlen(userInput);

		if (bytes_read > 0)
		{
			int sendRes = SendData(&clientSocket, userInput, bytes_read + 1);
			if (sendRes == -1)
			{
				printf("Could not send to server! Whoops!\r\n");
				continue;
			}

			memset(buf, 0, BUF_SIZE);
			int bytesReceived = ReceiveData(&clientSocket, buf);
			if (bytesReceived == -1)
			{
				//cout << "There was an error getting response from server\r\n";
				printf("There was an error getting response from server\r\n");
			}

			if (bytesReceived > 0)
			{
				printf("CLIENT> ");
				//printf("Bytes received %d\n", bytesReceived);
				PrintMessage(buf, bytesReceived);
			}
		}

	} while (bytes_read > 0);

	// Close the socket
	CloseSocket(&clientSocket, 1);

	return 0;
}
